import React, { Component } from "react";
import Login from "./login.js";
import LoginProfil from "./loginProfil.js";
import Profil from "./profil.js";
import UbahProfil from "./ubahProfil.js";
import UbahPassword from "./ubahPassword.js";
import Riwayat from "../tabRiwayat/index.js";
import Bantuan from "./bantuan.js";
import BeforeLogin from "./beforeLogin.js";
import BerandaDetail from "./berandaDetail.js";
import Tambaheviden from "../tambaheviden/index1.js";
import DeskripsiEvidance from "./deskripsiEvidence.js";
import Beranda from "./HomeScreen.js";
import Akun from "./akun.js";
import SMS from "./sms.js";
import Proses from "../tabRiwayat/proses.js";
import Selesai from "../tabRiwayat/selesai.js";
import Coba from "./coba.js";
import ThankYou from "./ThankYou.js"
import ReturnLaporan from "./returnLaporan.js";
import DetailLaporanSelesai from "../tabRiwayat/detailLaporanSelesai.js";
import DetailLaporanProses from "../tabRiwayat/detailLaporanProses.js";
import LihatBukti from "../tabRiwayat/lihatBukti.js";
import SearchPage from "./SearchPage.js";
import SearchResult from "./SearchResult.js"



//import BeforeLogin from "./beforeLogin.js";
import { StackNavigator } from "react-navigation";
export default (DrawNav = StackNavigator({
    BeforeLogin: {
        screen: BeforeLogin,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    Login: {
        screen: Login,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null       
        }
    },
    LoginProfil: {
        screen: LoginProfil,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },
    Profil: {
        screen: Profil,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },
    UbahProfil: {
        screen: UbahProfil,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },
    UbahPassword: {
        screen: UbahPassword,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },

    Beranda: {
        screen: Beranda,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },
    Riwayat: {
        screen: Riwayat,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },


    BerandaDetail: {
        screen: BerandaDetail,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },
    Tambaheviden: {
        screen: Tambaheviden,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },
    DeskripsiEvidance: {
        screen: DeskripsiEvidance,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },
    SMS: {
        screen: SMS,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },
    Proses: {
        screen: Proses,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },
    Selesai: {
        screen: Selesai,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },
    Coba: {
        screen: Coba,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },

    Bantuan: {
        screen: Bantuan,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },

    ReturnLaporan: {
        screen: ReturnLaporan,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },

    ThankYou: {
        screen: ThankYou,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },
    DetailLaporanSelesai: {
        screen: DetailLaporanSelesai,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },

    DetailLaporanProses: {
        screen: DetailLaporanProses,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },

    LihatBukti: {
        screen: LihatBukti,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },
    SearchPage: {
        screen: SearchPage,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },
    SearchResult: {
        screen: SearchResult,
        headerMode: 'none',
        header: null,
        navigationOptions: {
            header: null
        }
    },

}));