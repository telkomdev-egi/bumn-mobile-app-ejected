import React from "react";
import { StatusBar, Image, MapView, AsyncStorage, Alert, TextInput, ImageBackground, View, StyleSheet } from "react-native";
import {
    Button,
    Text,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Footer,
    FooterTab,
    H1,
    Thumbnail,
    Form,
    Item,
    Label,
    Input,
    List,
    ListItem,
    Tab,
    Tabs,


} from "native-base";

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import call from 'react-native-phone-call'

import Accordion from 'react-native-collapsible/Accordion';


const SECTIONS = [
    {
        title: 'Dari manakah saya bisa mendapatkan akun login petugas BUMN?',
        content: 'Anda akan mendapatkan akun login dari Command Center',
    },
    {
        title: 'Jika saya tidak bisa login, siapa yang harus saya hubungi?',
        content: 'Anda bisa menghubungi ke : 021-3860500',
    },
    {
        title: 'Apa saja yang bisa saya lakukan ketika saya sudah login sebagai petugas Sobat BUMN?',
        content: 'Anda akan menerima laporan kejadian yang telah dikirimkan oleh pelapor lalu mensolusikannya ke petugas lapangan, hingga masalah selesai.',
    },
    {
        title: 'Jika laporan tidak sesuai dengan tag kategori BUMN, langkah apa yang harus saya lakukan?',
        content: 'Anda bisa mengklik menu "return laporan" lalu akan diterima oleh tim Command Center dan akan ditindak lanjuti oleh BUMN terkait',
    }
    ,
    {
        title: 'Apakah saya bisa mengganti password akun yang saya dapat?',
        content: 'Anda bisa mengubah password di menu "Akun"',
    }
];

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#F5FCFF',
    },
    title: {
        textAlign: 'center',
        fontSize: 18,
        fontWeight: '300',
        marginBottom: 20,
    },
    header: {
        backgroundColor: '#F5FCFF',
        padding: 10,

    },
    headerText: {
        textAlign: 'left',
        fontSize: 14,
        fontWeight: '500',
    },
    content: {
        fontSize: 12,
        padding: 20,
        backgroundColor: '#fff',
    },
    active: {
        backgroundColor: 'rgba(255,255,255,1)',
    },
    inactive: {
        backgroundColor: 'rgba(245,252,255,1)',
    },
    selectors: {
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    selector: {
        backgroundColor: '#F5FCFF',
        padding: 10,
    },
    activeSelector: {
        fontWeight: 'bold',
    },
    selectTitle: {
        fontSize: 12,
        fontWeight: '500',
        padding: 10,
    },
    imgFooter: {
        height: "40%",
        width: "22%",
        marginBottom: "5%"
    },
    footer: {
        backgroundColor: "#fff"
    },
    fontFooter: {
        color: "#000",
        fontSize: responsiveFontSize(1.5)
    }
});

const args = {
  number: '0822253832652', // String value with the number to call
  prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
}


const args2 = {
  number: '081318522238', // String value with the number to call
  prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
}

export default class Bantuan extends React.Component {

    constructor(props) {
        super(props)
    }

    
    _renderHeader(section) {
        return (
            <View style={styles.header}>
                <Text style={styles.headerText}>{section.title}</Text>
            </View>
        );
    }

    _renderContent(section) {
        return (
            <View style={styles.content}>
                <Text>{section.content}</Text>
            </View>
        );
    }


    render() {
        return (

            <Container>
                {/*<Header style={{ alignItems: "center", backgroundColor: "white",  }}>


                    <Title style={{ color: "black" }}>FAQ</Title>

                </Header>
               */}

                <Tabs initialPage={0} tabBarUnderlineStyle={{ borderBottomWidth: 2, borderTopWidth: 0 }} style={{  }}>
                    <Tab heading="Bantuan" tabStyle={{ backgroundColor: 'white' }} textStyle={{ color: '#afafaf' }} activeTabStyle={{ backgroundColor: 'white' }} activeTextStyle={{ color: '#7282ff', fontWeight: 'bold' }}>
                        <Container>

                            <Content>
                                <View style={{justifyContent:"center", alignItems:"center",alignContent:"center"}}>
                                <Image style={{width:200,height:150,marginTop:"10%"}}
                                    source={require('../../img/asset/logo2.png')}
                                />
                                <Text style={{marginTop:"20%"}}>Helpdesk Sobat BUMN</Text>
                                

                                <Text style={{marginTop:"10%"}}> Prita  <Text onPress={this.callHelp} style={{textDecorationLine:"underline",color:"blue"}}>0822253832652</Text></Text>
                                <Text style={{marginTop:"5%"}}> Fathia  <Text onPress={this.callHelp2} style={{textDecorationLine:"underline",color:"blue"}}>081318522238</Text></Text>
                                </View>
                                

                                



                                {/*<Card style={{ backgroundColor: 'powderblue', paddingTop: "2%", marginRight: "5%", marginTop: "5%", marginLeft: "5%", height: responsiveWidth(20) }}>
                    <CardItem style={{ backgroundColor: 'powderblue' }}>
                        <Left>
                            <Thumbnail square small
                                source={require("../../img/asset/ic-email-send.png")}
                            />
                            <Body>
                                <Text style={{ fontSize: responsiveFontSize(1.5), marginLeft: "10%" }}>Butuh Bantuan?</Text>
                                <Text style={{ fontSize: responsiveFontSize(1.5), marginLeft: "10%",color:"#004ecc", fontWeight:"bold" }}>Silahkan kirim kekami.</Text>
                            </Body>
                        </Left>
                    </CardItem>
                </Card>
                <Input placeholder='Subjek' style={{backgroundColor:"#e8e8e8",marginLeft:"5%",marginRight:"5%",marginTop:"5%",borderRadius:2,paddingLeft:"5%"}}/>
                <Input placeholder='Isi' multiline={true} numberOfLines={7} style={{textAlignVertical: "top" ,backgroundColor:"#e8e8e8",marginLeft:"5%",marginRight:"5%",marginTop:"2%",borderRadius:2,paddingLeft:"5%",paddingTop:"5%"}}/>
                <Button style={{borderRadius:2,backgroundColor:"#004ecc",marginLeft:"10%",width:"80%",marginTop:"10%",flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <Text>Kirim</Text>
                </Button>
                 <Text style={{ fontSize: responsiveFontSize(1.5),textAlign:"center",marginTop:"5%" }}>atau hubungi <Text style={{ fontSize: responsiveFontSize(1.5), color:"#004ecc", fontWeight:"bold" }}>contact center kami.</Text></Text>
                                     */}

                            </Content>
                        </Container>
                    </Tab>

                    <Tab heading="FAQ" tabStyle={{ backgroundColor: 'white' }} textStyle={{ color: '#afafaf',fontSize:12 }} activeTabStyle={{ backgroundColor: 'white' }} activeTextStyle={{ color: '#7282ff', fontWeight: 'bold' }}>
                        <Container>


                            <Content>
                                <Accordion
                                    sections={SECTIONS}
                                    renderHeader={this._renderHeader}
                                    renderContent={this._renderContent}
                                />

                            </Content>



                        </Container>
                    </Tab>
                </Tabs>



                <Footer>
                    <FooterTab style={styles.footer}>
                        <Button
                            onPress={() => this.props.navigation.navigate("Beranda")}
                        >

                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/navbar/ico-tabs-home-1.png")}
                            />

                            <Text uppercase={false} style={styles.fontFooter}>Beranda</Text>
                        </Button>
                        <Button
                            onPress={() => this.props.navigation.navigate("Riwayat")}
                        >
                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/navbar/ico-tabs-riwayat-1.png")}
                            />
                            <Text uppercase={false} style={styles.fontFooter}>Riwayat</Text>
                        </Button>
                        <Button
                            vertical
                        >
                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/navbar/ico-tabs-help.png")}
                            />
                            <Text uppercase={false} style={styles.fontFooter}>Bantuan</Text>
                        </Button>
                        <Button
                            onPress={() => this.props.navigation.navigate("Profil")}
                        >
                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/navbar/ico-tabs-profile-1.png")}
                            />
                            <Text uppercase={false} style={styles.fontFooter}>Akun</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
    callHelp =()=>{
        <Button onPress={call(args).catch(console.error)}>Test</Button>
    }

    callHelp2 =()=>{
        <Button onPress={call(args2).catch(console.error)}>Test</Button>
    }

}


