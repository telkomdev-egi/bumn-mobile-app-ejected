import React, { Component } from "react";
import { StatusBar, Image, View, MapView, StyleSheet, TouchableHighlight, Alert,TouchableOpacity, AsyncStorage } from "react-native";
import {
  Tab,
  Tabs,
  Button,
  Text,
  Container,
  Card,
  CardItem,
  Body,
  Content,
  Header,
  Title,
  Left,
  Icon,
  Right,
  Footer,
  FooterTab,
  H1,
  Thumbnail,
  Form,
  Item,
  Label,
  Input,
  H2,
	List,
	ListItem,
} from 'native-base';


import Selesai from "./selesai.js";
import Proses from "./proses.js";

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

export default class RentStatus extends React.Component {

    constructor(props) {
		super(props)
		this.state = {
			dataAktif: [],
			dataSewaAktif: [],
			dataBarang: [],
			dataSewaAktifDetail: [],
			username: "",
			dataProses : [],
            dataSelesai: [],
            page : "1"
		}
	}

    componentDidMount() {
        AsyncStorage.getItem("bumnId", (error, result) => {
		 fetch("http://ec2-13-250-62-76.ap-southeast-1.compute.amazonaws.com:7000/api/v1/laporan/byStatusProses", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                instansi : result,
                page : "1"
            })
        })
            .then(response => response.json())
            .then((data) => {
                console.log(data.response);
                this.setState({
							dataProses: data.response

						});
						console.log(this.state.dataProses)
            })

            fetch("http://ec2-13-250-62-76.ap-southeast-1.compute.amazonaws.com:7000/api/v1/laporan/byStatusSolved", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                instansi : result,
                page : "1"
            })
        })
            .then(response => response.json())
            .then((data) => {
                console.log(data.response);
                this.setState({
							dataSelesai: data.response

						});
						console.log(this.state.dataSelesai)

            })
        })

	}

  render() {
    return (
       <Container>
        <Header style={{ backgroundColor: "white",  justifyContent: "center", alignItems: "center", shadowColor: 'transparent'}}>
         
            <Title style={{ color: "black" }}>Riwayat</Title>

        </Header>

        <Tabs initialPage={0} tabBarUnderlineStyle={{ borderBottomWidth: 2, borderTopWidth:0 }}>
          <Tab heading="Dalam Proses" tabStyle={{ backgroundColor: 'white' }} textStyle={{ color: '#afafaf' }} activeTabStyle={{ backgroundColor: 'white' }} activeTextStyle={{ color: '#7282ff', fontWeight: 'bold' }}>
            <Container>

				<Content>
                     
					<Card style={{ backgroundColor: 'powderblue', marginRight: "5%", marginTop: "5%", marginLeft: "5%" }}>
						<CardItem style={{ backgroundColor: 'powderblue' }}>
                            
							<Left>
								<Thumbnail square small
									source={require("../../img/asset/ic-lamp.png")}
								/>
								<Body>
									<Text style={{ fontSize: responsiveFontSize(1.5), fontStyle: "italic", marginLeft: "10%" }}>Anda dapat mengupdate status laporan kapanpun dengan mengupload eviden pengerjaan.</Text>

								</Body>
							</Left>
                           
						</CardItem>
					</Card>
                    {
                                this.state.dataProses.length == 0 ?
                            <View style ={{marginTop:"10%", alignItems: "center" }}>
                                <Text style={{color:"#cccccc"}}>Belum Ada Laporan Masuk </Text>
                                </View>:null
                        }

 {                      this.state.dataProses.length !=0 ?
							this.state.dataProses.map((item, index) => (
					<List key={item.nomor_laporan}>
				
						<ListItem  onPress={() => this.props.navigation.navigate("DetailLaporanProses", { nomor_laporan: item.nomor_laporan })} style={{borderBottomWidth: 0}}>

							<Left>
							  {   item.avatar_pelapor==null ?
								<Thumbnail small
									source={require("../../img/asset/Profil.png")}
								/>:null
                             }
                              {   item.avatar_pelapor!=null ?
								<Thumbnail small
									source={{uri : item.avatar_pelapor}}
								/>:null
                             }
								<Body>
									<Text style={{ fontSize: responsiveFontSize(2) }}>{item.pelapor}</Text>
									<Text style={{ fontSize: responsiveFontSize(1.5) }}>No. Tiket : {item.nomor_laporan}</Text>
								</Body>
							</Left>
							
							<Right>
								<Text style={{ fontSize: responsiveFontSize(1.5),fontStyle:"italic" }}>{item.tanggal_laporan}</Text>
							</Right>
				
					
						</ListItem>
                        
						
						
						<ListItem style={{marginTop:"-5%",marginRight:"5%"}} >
							<Text style={{fontSize: responsiveFontSize(2)}}>{item.description}</Text>
						</ListItem>
						
						
					</List>
					 )):null
						} 
					
					

					

					
				</Content>
			</Container>
          </Tab>
          <Tab heading="Selesai" tabStyle={{ backgroundColor: 'white' }} textStyle={{ color: '#afafaf' }} activeTabStyle={{ backgroundColor: 'white' }} activeTextStyle={{ color: '#7282ff', fontWeight: 'bold' }}>
            	<Container>

				<Content>
					<Card style={{ backgroundColor: 'powderblue',paddingTop:"2%", marginRight: "5%", marginTop: "5%", marginLeft: "5%",height:responsiveWidth(20) }}>
						<CardItem style={{ backgroundColor: 'powderblue' }}>
							<Left>
								<Thumbnail square small
									source={require("../../img/asset/ic-party.png")}
								/>
								<Body>
									<Text style={{ fontSize: responsiveFontSize(1.5),  marginLeft: "10%" }}>Selamat! berikut tiket pelaporan Anda yang berstatus <Text style={{fontWeight:"bold"}}>Solved.</Text> </Text>
								

								</Body>
							</Left>
						</CardItem>
					</Card>

                    {
                                this.state.dataSelesai.length == 0 ?
                            <View style ={{marginTop:"10%", alignItems: "center" }}>
                                <Text style={{color:"#cccccc"}}>Belum Ada Laporan Masuk </Text>
                                </View>:null
                        }


{                       this.state.dataSelesai.length != 0 ?
							this.state.dataSelesai.map((item, index) => (
					
						
						<View key={item.nomor_laporan}>
                        <List >
                        <ListItem onPress={() => this.props.navigation.navigate("DetailLaporanSelesai", { nomor_laporan: item.nomor_laporan })} style={{borderBottomWidth: 0}}>
			
							<Left>
                             {   item.avatar_pelapor==null ?
								<Thumbnail small
									source={require("../../img/asset/Profil.png")}
								/>:null
                             }
                              {   item.avatar_pelapor!=null ?
								<Thumbnail small
									source={{uri : item.avatar_pelapor}}
								/>:null
                             }
								<Body>
									<Text style={{ fontSize: responsiveFontSize(2) }}>{item.pelapor}</Text>
									<Text style={{ fontSize: responsiveFontSize(1.5) }}>No. Tiket : {item.nomor_laporan}</Text>
								</Body>
							</Left>
							<Right>
								<Text style={{ fontSize: responsiveFontSize(1.5),fontStyle:"italic" }}>{item.tanggal_laporan}</Text>
							</Right>
				

						</ListItem>
						
						
						<ListItem style={{marginTop:"-5%",marginRight:"5%"}} >
							<Text style={{fontSize: responsiveFontSize(2)}}>{item.description}</Text>
						</ListItem>
						
					</List>
                    </View>
                   
					)):null
						} 

					
				</Content>
			</Container>
          </Tab>

          
        </Tabs>
        <Footer>
                    <FooterTab style={styles.footer}>
                        <Button
                            onPress={() => this.props.navigation.navigate("Beranda")}
                        >

                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/navbar/ico-tabs-home-1.png")}
                            />

                            <Text uppercase={false} style={styles.fontFooter}>Beranda</Text>
                        </Button>
                        <Button
                           
                        >
                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/navbar/ico-tabs-riwayat.png")}
                            />
                            <Text uppercase={false} style={styles.fontFooter}>Riwayat</Text>
                        </Button>
                        <Button
                            vertical
                            onPress={() => this.props.navigation.navigate("Bantuan")}
                        >
                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/navbar/ico-tabs-help-1.png")}
                            />
                            <Text uppercase={false} style={styles.fontFooter}>Bantuan</Text>
                        </Button>
                        <Button
                            onPress={() => this.props.navigation.navigate("Profil")}
                        >
                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/navbar/ico-tabs-profile-1.png")}
                            />
                            <Text uppercase={false} style={styles.fontFooter}>Akun</Text>
                        </Button>
                    </FooterTab>
                </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
    imgFooter: {
        height: "40%",
        width: "22%",
        marginBottom: "5%"
    },
    footer: {
        backgroundColor: "#fff"
    },
    fontFooter: {
        color: "#000",
        fontSize: responsiveFontSize(1.5)
    }
})