import React, { Component } from "react";
import {
  ActivityIndicator,
  Clipboard,
  Image,
  Share,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  AsyncStorage,
  Alert
} from 'react-native';
import {
  Tab,
  Button,
  Tabs,
  Container,
  Card,
  CardItem,
  Body,
  Content,
  Header,
  Title,
  Left,
  Icon,
  Right,
  Footer,
  FooterTab,
  H1,
  Thumbnail,
  Form,
  Item,
  Label,
  Input,
  TextInput
} from 'native-base';
//import Exponent, { Constants, ImagePicker, registerRootComponent } from 'expo';
import base64 from 'base-64';
import utf8 from 'utf8';
import Modal from 'react-native-modal';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import Spinner from 'react-native-loading-spinner-overlay';

export default class Detail1 extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      dataUser: [],
      userId: "",
      nomor_laporan: this.props.navigation.state.params.nomor_laporan,
      imagesdata: this.props.navigation.state.params.imagesdata,
      komentar: '',
      uploading: false,
      isModalVisible: false,
      visible: false,
      name: "",
      dataLaporan : []
    }
  }

  componentDidMount() {
    fetch("http://ec2-13-250-62-76.ap-southeast-1.compute.amazonaws.com:7000/api/v1/laporan/findById/" + this.state.nomor_laporan, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then((data) => {
        console.log(data.response);
        this.setState({
          dataLaporan: data.response[0]

        });
      })
    AsyncStorage.getItem("userid", (error, result) => {
      if (result) {
        console.log("userid : " + result)
      }
      this.state.userId = result;
      fetch("http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/profile/" + result, {
        method: "GET",
        headers: {
          'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
        }
      })
        .then((response) => response.json())
        .then((data) => {

          this.setState({
            dataUser: data.data,
          });
          this.state.name = data.data.name;
          console.log(this.state.dataUser)
        })
        .catch((error) => {
          console.log(error);
        })
    })
  }

  _showModal = () => {
    console.log(this.state.imagesdata)
  }

  _hideModal = () => {
    this.setState({ isModalVisible: false });
    this.props.navigation.navigate("Beranda")
  }


  render() {
    const { goBack } = this.props.navigation;
    console.log("datauser: " + this.state.userId)
    console.log(this.state.imagesdata)
    return (
      <Container>
        <Header style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff' }}>
          <Left>
            <Button
              transparent
              onPress={() => goBack(null)}
            >
              <Icon style={{ color: "#000" }} name="arrow-back" />
            </Button>
          </Left>
          <Title style={{ color: "#000", alignItems: "center" }}>Deskripsi Eviden</Title>
          <Right />
        </Header>
        <Content>
          <Card>
            <View style={{ width: '100%', height: '10%', flexDirection: "row", justifyContent: 'space-between', padding: 10 }}>
              <Image style={{ marginLeft: 5, width: 100, height: 100 }} source={{ uri: this.state.imagesdata }} />
              <Input style={{ textAlignVertical: "top", height: 130, marginLeft: 5 }} placeholder="Tulis komentar anda" onChangeText={this.handleKomentar} multiline={true} numberOfLines={6} />
            </View>
          </Card>
          <View style={{ flex: 1 }}>
            <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />
          </View>
          <Button
            style={{ backgroundColor: "#004ecc", marginLeft: '10%', width: "80%", marginTop: '70%', flex: 1, justifyContent: 'center', alignItems: 'center' }}
            transparent
            onPress={this._handleImagePicked}
          >
            <Text style={{ color: '#fff' }}>Kirim</Text>
          </Button>
        </Content>
        <Modal isVisible={this.state.isModalVisible}>
          <Thumbnail large source={require("../../img/asset/likes-bulat.png")}
            style={{ zIndex: 3, marginTop: "-70%", marginRight: "30%", marginLeft: "37%" }} />
          <Content padder style={{ position: "absolute", flex: 1, backgroundColor: 'rgba(255, 255, 255, 1)', marginRight: "10%", marginLeft: "10%", marginTop: "50%", marginBottom: "50%", borderRadius: 10 }}  >
            <Label style={{ marginLeft: "14%", textAlign: 'center', fontSize: responsiveFontSize(2), color: "black", marginTop: "25%", width: "70%" }}>
              Terima kasih atas bantuan Anda!</Label>
            <Label style={{ marginLeft: "14%", textAlign: 'center', fontSize: responsiveFontSize(1.5), color: "black", marginTop: "4%", width: "70%" }}>
              Selanjutnya Eviden ini akan diteruskan kepada pelapor</Label>
            <Button onPress={this._hideModal} style={{ backgroundColor: "#0052A8", flex: 1, justifyContent: 'center', alignItems: 'center', marginLeft: "20%", marginRight: "25%", marginTop: "15%", width: responsiveWidth(40), borderRadius: 10 }}>
              <Text style={{ color: "#fff" }}> Oke </Text></Button>
          </Content>
        </Modal>
      </Container>
    );
  }

  handleKomentar = (text) => {
    this.setState({ komentar: text })
  }

  _maybeRenderUploadingOverlay = () => {
    if (this.state.uploading) {
      return (
        <View
          style={[
            StyleSheet.absoluteFill,
            {
              backgroundColor: 'rgba(0,0,0,0.4)',
              alignItems: 'center',
              justifyContent: 'center',
            },
          ]}>
          <ActivityIndicator color="#fff" animating size="large" />
        </View>
      );
    }
  };

  _maybeRenderImage = () => {
    let { image } = this.state;
    if (!image) {
      return;
    }

    return (
      <View
        style={{
          marginTop: 30,
          width: 250,
          borderRadius: 3,
          elevation: 2,
          shadowColor: 'rgba(0,0,0,1)',
          shadowOpacity: 0.2,
          shadowOffset: { width: 4, height: 4 },
          shadowRadius: 5,
        }}>
        <View
          style={{
            borderTopRightRadius: 3,
            borderTopLeftRadius: 3,
            overflow: 'hidden',
          }}>
          <Image source={{ uri: image }} style={{ width: 250, height: 250 }} />
        </View>

        <Text
          onPress={this._copyToClipboard}
          onLongPress={this._share}
          style={{ paddingVertical: 10, paddingHorizontal: 10 }}>
          {image}
        </Text>
      </View>
    );
  };

  _share = () => {
    Share.share({
      message: this.state.image,
      title: 'Check out this photo',
      url: this.state.image,
    });
  };

  _copyToClipboard = () => {
    Clipboard.setString(this.state.image);
    alert('Copied image URL to clipboard');
  };

  _handleImagePicked = async () => {

    if (this.state.komentar == null || this.state.komentar == '') {
      Alert.alert(
        'Pesan',
        'Harap masukan komentar',
        [
          //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
          //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          { text: 'OK', style: 'cancel' },
        ],
        { cancelable: false }
      )
    } else {
      this.setState({
        visible: !this.state.visible
      });
      console.log('_handleImagePicked: ', this.state.nomor_laporan);
      let uploadResponse, uploadResult;

      try {
        this.setState({ uploading: true });

        if (this.state.imagesdata != undefined) {
          uploadResponse = await uploadImageAsync(this.state.imagesdata, this.state.komentar, this.state.nomor_laporan, this.state.userId);
          uploadResult = await uploadResponse.json();
          this.setState({ image: uploadResult.location });
          //update images 

          return fetch("http://ec2-13-250-62-76.ap-southeast-1.compute.amazonaws.com:7000/api/v1/laporan/updateStatus/" + this.state.nomor_laporan, {
            method: 'PUT',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              status: 'selesai',
              updater: this.state.name,
              id_pelapor : this.state.dataLaporan.id_pelapor
            })
          })
            .then(response => response.json())
            .then((data) => {
              if (data.status != "200") {
                Alert.alert(
                  'Pesan',
                  'Upload Gagal',
                  [
                    //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                    //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    { text: 'OK', style: 'cancel' },
                  ],
                  { cancelable: false }
                )
              } else {
                this.setState({ isModalVisible: true })
                this.setState({
                  visible: false
                });
                console.log('data: ', this.state.nomor_laporan);

              }
            })
        }

      }
      finally {
        this.setState({ uploading: false });
      }
    };
  }
}

async function uploadImageAsync(uri, komentar, nomor_laporan, id_login) {
  let apiUrl = 'http://ec2-13-250-62-76.ap-southeast-1.compute.amazonaws.com:3000/uploads1';


  // Note:
  // Uncomment this if you want to experiment with local server
  //
  // if (Constants.isDevice) {
  //   apiUrl = `https://your-ngrok-subdomain.ngrok.io/upload`;
  // } else {
  //   apiUrl = `http://localhost:3000/upload`
  // }


  let formData = new FormData();
  formData.append('nomor_laporan', nomor_laporan);
  formData.append('images_evidance', {
    uri,
    name: `photo.png`,
    type: `image/png`,
  });
  formData.append('description_evidance', komentar);
  formData.append('id_login', id_login);

  console.log(formData)
  let options = {
    method: 'POST',
    body: formData,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
  };

  return fetch(apiUrl, options);
}