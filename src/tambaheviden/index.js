import React, { Component } from "react";
import {
  ActivityIndicator,
  Clipboard,
  Image,
  Share,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import {
  Tab,
  Button,
  Tabs,
  Container,
  Card,
  CardItem,
  Body,
  Content,
  Header,
  Title,
  Left,
  Icon,
  Right,
  Footer,
  FooterTab,
  H1,
  Thumbnail,
  Form,
  Item,
  Label,
  Input,
  AsyncStorage
} from 'native-base';
//import Exponent, { Constants, ImagePicker, registerRootComponent } from 'expo';
import ImagePicker from 'react-native-image-crop-picker';



export default class Index2 extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      nomor_laporan: this.props.navigation.state.params.nomor_laporan,
      kamera: true,
      tombolgaleri: true,
      uploading: false
    }
  }


  render() {
    let { image } = this.state;
    let { galeri } = this.state;
    const { goBack } = this.props.navigation;
    return (
      <Container>
        <Header style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff' }}>
          <Left>
            <Button
              transparent
              onPress={() => goBack(null)}
            >
              <Icon style={{ color: "#000" }} name="arrow-back" />
            </Button>
          </Left>
          <Title style={{ color: "#000", alignItems: "center" }}>Tambah Eviden</Title>
          <Right>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("Detail", { imagesdata: "https://s3-ap-southeast-1.amazonaws.com/sobatbumn/evidence/no-image.png", nomor_laporan: this.state.nomor_laporan })}
            >
              <Text style={{ color: "#000", alignItems: "center" }}>Skip</Text>
            </Button>
          </Right>
        </Header>
        <Tabs initialPage={0} tabBarUnderlineStyle={{ borderBottomWidth: 2 }}>
          <Tab heading="Kamera" tabStyle={{ backgroundColor: 'white' }} textStyle={{ color: '#012' }} activeTabStyle={{ backgroundColor: '#fff' }} activeTextStyle={{ color: '#00f', fontWeight: 'normal' }}>

            <Container>
              <View style={{ width: '100%', height: '50%' }}>
                {image &&
                  <Image style={{ width: '100%', height: '95%' }} source={{ uri: image }} />}
              </View>
              <View style={{ flex: 1, alignItems: 'center' }}>
                {
                  this.state.kamera ?
                    <View >
                      <TouchableOpacity onPress={this._takePhoto}>
                        <Image style={{ width: 83, height: 83 }}
                          source={require("../../img/asset/kamera.png")} />
                      </TouchableOpacity>
                    </View> : null
                }
              </View>
              {
                this.state.image ?
                  <View style={{ alignItems: 'center', bottom: '20%' }}>
                    <Text style={{ fontSize: 17 }}> Gunakan foto ini? </Text>
                  </View> : null
              }
              {
                this.state.image ?
                  <View style={{ flexDirection: "row", flex: 1, bottom: '25%', left: 0, right: 0, justifyContent: 'space-between', padding: 15 }}>
                    <Button
                      style={{ width: '45%', justifyContent: 'center', border: '5%' }}
                      onPress={() => this.props.navigation.navigate("Detail", { imagesdata: this.state.image, nomor_laporan: this.state.nomor_laporan })}>
                      <Text style={{ fontSize: 17, color: '#fff' }}> Gunakan</Text>
                    </Button>
                    <Button bordered primary
                      style={{ width: '45%', justifyContent: 'center', borderWidth: 1, borderColor: 'blue' }}
                      light onPress={this._takePhoto}>
                      <Text style={{ fontSize: 17, color: '#00f' }}> Ambil Lagi</Text>
                    </Button>
                  </View> : null
              }
            </Container>
          </Tab>
          <Tab heading="Galeri" tabStyle={{ backgroundColor: 'white' }} textStyle={{ color: '#012' }} activeTabStyle={{ backgroundColor: '#fff' }} activeTextStyle={{ color: '#00f', fontWeight: 'normal' }}>
            <View style={{ width: '100%', height: '50%' }}>
              <Image style={{ width: '100%', height: '95%' }} source={{ uri: this.state.galeri }} />
            </View>
            {
              this.state.tombolgaleri ?
                <Button
                  style={{ backgroundColor: "#004ecc", marginLeft: '10%', bottom: 0, width: "80%", justifyContent: 'center', alignItems: 'center' }}
                  transparent onPress={this._pickImage}>
                  <Text style={{ color: '#fff' }}>Pick an image from Galery</Text>
                </Button> : null
            }

            {
              this.state.galeri ?
                <View style={{ alignItems: 'center', bottom: 0 }}>
                  <Text style={{ fontSize: 20 }}> Gunakan foto ini? </Text>
                </View> : null
            }
            {
              this.state.galeri ?
                <View style={{ flexDirection: "row", flex: 1, bottom: 0, left: 0, right: 0, justifyContent: 'space-between', padding: 15 }}>
                  <Button
                    style={{ width: '45%', justifyContent: 'center', border: '5%' }}
                    onPress={() => this.props.navigation.navigate("Detail", { imagesdata: this.state.galeri, nomor_laporan: this.state.nomor_laporan })}>
                    <Text style={{ fontSize: 17, color: '#fff' }}> Gunakan</Text>
                  </Button>
                  <Button bordered primary
                    style={{ width: '45%', justifyContent: 'center', borderWidth: 1, borderColor: 'blue' }}
                    light onPress={this._pickImage}>
                    <Text style={{ fontSize: 17, color: '#00f' }}> Ambil Lagi</Text>
                  </Button>
                </View> : null
            }
          </Tab>
        </Tabs>
      </Container>
    );
  }

  _takePhoto = async () => {
    console.log('nolaporan' + this.state.nomor_laporan);
    ImagePicker.openCamera({
      width: 400,
      height: 300,
      cropping: true
    }).then(image => {
      console.log(image.path);
       if (image.path != undefined) {
         this.setState({ kamera: false })
       } 
       else {
         this.setState({ kamera: true })
       }
      this.setState({
        image: image.path
      })
    });
    // let pickerResult = await ImagePicker.launchCameraAsync({
    //   allowsEditing: true,
    //   aspect: [4, 3],
    // });

  };

  _pickImage = async () => {
    // let result = await ImagePicker.launchImageLibraryAsync({
    //   allowsEditing: true,
    //   aspect: [4, 3],
    // });
   
    ImagePicker.openPicker({
      width: 400,
      height: 300,
      cropping: true
    }).then(image => {
      console.log(image.path);
       if (image.path != undefined) {
         this.setState({ tombolgaleri: false })
       } 
       else {
         this.setState({ tombolgaleri: true })
       }
      this.setState({
        galeri: image.path
      })
    });
  };
}